export const PARROT_TYPES = {
    EUROPEAN: 'EUROPEAN',
    AFRICAN: 'AFRICAN',
    NORWEGIAN_BLUE: 'NORWEGIAN_BLUE',
};

export class Parrot {
    constructor(parrot) {
        this.calcularVelocidad=new CalcularVelocidad(parrot);
    }
    getSpeed(){
        return this.calcularVelocidad.getSpeed();
    }
}

export class African{
    constructor(type,numberOfCoconuts) {
        this.type=type;
        this.numberOfCoconuts=numberOfCoconuts;
    }
}

export class Norwegian_Blue  {
    constructor(type, voltage, isNailed) {
        this.type=type;
        this.voltage=voltage;
        this.isNailed=isNailed;
    }
}

export class European {
    constructor(type) {
        this.type=type;
    }
}

export class CalcularVelocidad{
    constructor(parrot) {
        this.loadFactor = 9;
        this.baseSpeed = 12;
        this.parrot = parrot;
    }

    getSpeed() {
        switch (this.parrot.type) {
            case PARROT_TYPES.EUROPEAN:
                return this.baseSpeed;
            case PARROT_TYPES.AFRICAN:
                return Math.max(0, this.baseSpeed - this.loadFactor * this.parrot.numberOfCoconuts);
            case PARROT_TYPES.NORWEGIAN_BLUE:
                return (this.parrot.isNailed) ? 0 : this.getBaseSpeedWithVoltage(this.parrot.voltage);
        }
        throw new Error("Should be unreachable");
    }

    getBaseSpeedWithVoltage(voltage) {
        return Math.min(24, voltage * this.baseSpeed);
    }
}