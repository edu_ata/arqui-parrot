import { Parrot, PARROT_TYPES,African,European,Norwegian_Blue,CalcularVelocidad } from '../parrot';

describe("Parrot", function () {
    test("get speed of european parrot", function () {
        const parrot = new Parrot(new European(PARROT_TYPES.EUROPEAN));
        expect(parrot.getSpeed()).toBe(12);
    });

    test("get speed of african parrot with one coconut", function () {
        const parrot = new Parrot(new African(PARROT_TYPES.AFRICAN,1));
        const interfaceSpeed= new CalcularVelocidad(parrot)
        expect(parrot.getSpeed()).toBe(3);
    });

    test("get speed of african parrot with two coconuts", function () {
        const parrot = new Parrot(new African(PARROT_TYPES.AFRICAN,2));
        expect(parrot.getSpeed()).toBe(0);
    });

    test("get speed of african parrot with no coconuts", function () {
        const parrot = new Parrot(new African(PARROT_TYPES.AFRICAN,0));
        expect(parrot.getSpeed()).toBe(12);
    });

    test("get speed norwegian blue parrot nailed", function () {
        const parrot = new Parrot(new Norwegian_Blue(PARROT_TYPES.NORWEGIAN_BLUE,1.5, true));
        expect(parrot.getSpeed()).toBe(0);
    });

    test("get speed norwegian blue parrot not nailed", function () {
        const parrot = new Parrot(new Norwegian_Blue(PARROT_TYPES.NORWEGIAN_BLUE,1.5, false));

        expect(parrot.getSpeed()).toBe(18);
    });

    test("get speed norwegian blue parrot not nailed high voltage", function () {
        const parrot = new Parrot(new Norwegian_Blue(PARROT_TYPES.NORWEGIAN_BLUE,4, false));

        expect(parrot.getSpeed()).toBe(24);
    });
});
